import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LoginComponent } from './core/components/login/login.component';
import { HeaderComponent } from './modules/header/header.component';
import { FooterComponent } from './modules/footer/footer.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { environment } from '../environments/environment';
import { AddUserComponent } from './modules/users-add-user/components/add-user/add-user.component';
import {AuthService} from './core/components/guards/auth/guards/auth.service';
import {AuthGuard} from './core/components/guards/auth/guards/auth-guards.service';
import {FormsEditGuard} from './core/components/guards/forms/guard/forms-edit-guard';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {ToastrModule} from 'ngx-toastr';

//TODO: place intoa a differnt module
import {MatButtonModule, MatCheckboxModule, MatTableModule} from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TestModule} from './testcomponent/test.module';
import {LandingPageRoutingModule} from './modules/landing-page/landing-page-routing.module';
import {TestRoutingModule} from './testcomponent/test-routing.module';
import {LandingPageModule} from './modules/landing-page/landing-page.module';
import {ResetLoginRoutingModule} from './modules/reset-login/reset-login-routing.module';
import {ResetLoginModule} from './modules/reset-login/reset-login.module';
import {ForgottenLoginModule} from './modules/forgotten-login/forgotten-login.module';
import {ForgottenLoginRoutingModule} from './modules/forgotten-login/forgotten-login-routing.module';
import {DashboardModule} from './modules/dashboard/dashboard.module';
import {DashboardRoutingModule} from './modules/dashboard/dashboard-routing.module';
import {FamilyRoutingModule} from './modules/family/family-routing.module';
import {FamilyModule} from './modules/family/family.module';
import {AddOrganisationRoutingModule} from './modules/add-organisations/add-organisation-routing.module';
import {AddOrganisationModule} from './modules/add-organisations/add-organisation.module';
import {AddUsersRoutingModule} from './modules/users-add/add-users-routing.module';
import {AddUsersModule} from './modules/users-add/add-users.module';
import {UsersAddUserModule} from './modules/users-add-user/users-add-user.module';
import {UsersAddUserRoutingModule} from './modules/users-add-user/users-add-user-routing.module';
import {UsersViewDetailsModule} from './modules/users-view-details/users-view-details.module';
import {UsersViewDetailsRoutingModule} from './modules/users-view-details/users-view-details-routing.module';
import {AdminRegistrationModule} from './modules/administration-registration-form/admin-registration.module';
import {AdminRegistrationRoutingModule} from './modules/administration-registration-form/admin-registration-routing.module';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function tokenGetter() {
  return localStorage.getItem('jwt');
}

console.log('tokenGetter: ', tokenGetter());

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    AddUserComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: [environment.API_WHITE_LISTED_DOMAIN]
      }
    }),
    AngularFontAwesomeModule,
    FormsModule,

    TestModule,
    TestRoutingModule,
    LandingPageModule,
    LandingPageRoutingModule,
    ResetLoginModule,
    ResetLoginRoutingModule,
    ForgottenLoginModule,
    ForgottenLoginRoutingModule,
    DashboardModule,
    DashboardRoutingModule,
    FamilyModule,
    FamilyRoutingModule,
    AddOrganisationModule,
    AddOrganisationRoutingModule,
    AddUsersModule,
    AddUsersRoutingModule,
    UsersAddUserModule,
    UsersAddUserRoutingModule,
    UsersViewDetailsModule,
    UsersViewDetailsRoutingModule,
    AdminRegistrationModule,
    AdminRegistrationRoutingModule,
    /*
    RouterModule.forRoot([
      {
        path: 'admin',

        // comment out during debugging
        canActivate: [AuthGuard],
        children: [
          { DONE ✔
            path: 'dashboard',
            component: DashboardComponent
          },
          { DONE ✔
            path: 'organisations',
            component: AddOrganisationsComponent
          },
          { DONE ✔
            path: 'family',
            component: FamilyComponent
          },
          // to manage and add companies, PYMES, and for PYMES to add their clients
          { DONE ✔
            path: 'user',
            component: UsersAddComponent, // was login form: UsersComponent
          },

          // TODO: READ-ONLY, finish generic form view
          // example: http://localhost:4200/#/admin/user/markwebley
          { DONE ✔
            path: 'user/:clientid',
            component: UsersViewDetailsComponent
          },

          // where clients can add users
          // http://localhost:4200/#/admin/user/markwebley/add-users
          { DONE ✔
            path: 'user/:clientid/add-users',
            component: UsersAddUserComponent
          },

          // TODO: same as add view BUT form auto populated
          { TODO: NEW
            path: 'user/:clientid/edit',
            component: TODO
          },
          {
            path: '',
            redirectTo: '/user',
            pathMatch: 'full'
          }

        ]
      },
      DONE ✔  {path: 'login', component: LandingPageComponent},
      {path: 'landing-page', component: LandingPageComponent}, // to replace the homepage later

      // http://localhost:4200/#/login-reset/7b62f6aa-c50a-4025-83ce-3481ce9d9355
      DONE ✔ {path: 'login-reset/:sessionId', component: ResetLoginComponent},
      DONE ✔ {path: 'login-reset', component: ResetLoginComponent},
      DONE ✔ {path: 'login-restore', component: ForgottenLoginView},
      {path: '', redirectTo: '/login', pathMatch: 'full'} // ,
      // {path: '**', redirectTo: 'PageNotFoundComponent TODO}
    ], {useHash: true, enableTracing: true}) // <-- enable tracing for debugging purposes only
    */
  ],
  providers: [AuthService, AuthGuard, FormsEditGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
