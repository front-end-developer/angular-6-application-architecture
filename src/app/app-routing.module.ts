import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LandingPageComponent} from './modules/landing-page/components/landing-page.component';

const routes: Routes = [
  { path: 'login', component: LandingPageComponent},
  { path: '', component: LandingPageComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
