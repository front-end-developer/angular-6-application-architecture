import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FamilyComponent} from './components/family.component';
import {AddOrganisationsComponent} from '../add-organisations/components/add-organisations.component';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';

const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'family',
        component: FamilyComponent
      }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class FamilyRoutingModule {}
