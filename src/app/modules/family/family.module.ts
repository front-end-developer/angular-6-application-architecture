import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
// import {ResetPasswordComponent} from '../../core/components/login-forgotten/reset-password/reset-password.component';
// import {LoginResetModel} from '../../core/components/models/login-reset.model';
import {FamilyComponent} from './components/family.component';
import {AssociatorComponent} from './components/associator/associator.component';
import {LookupsService} from '@mw-services/lookups/lookups.service';
import {MatButtonModule, MatCheckboxModule, MatTableModule} from '@angular/material';
import {AuthService} from '../../core/components/guards/auth/guards/auth.service';
import {ToastrService} from 'ngx-toastr';
import {FormsEditGuard} from '../../core/components/guards/forms/guard/forms-edit-guard';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';
import {UserService} from '@mw-services/user/user.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
  ],
  declarations: [
    AssociatorComponent,
    FamilyComponent
  ],
  entryComponents: [FamilyComponent],
  providers: [LookupsService, ToastrService, AuthService, AuthGuard, FormsEditGuard]
})
export class FamilyModule {} // <- MAKE SURE THIS NAME DOES NOT CLASH
