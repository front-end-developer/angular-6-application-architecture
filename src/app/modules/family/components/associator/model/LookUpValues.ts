export class LookUpValues {
  constructor(
    public lookupcode: string = '',
    public description: string = '',
    public status: string = ''
  /*
  createdby character varying(100) COLLATE pg_catalog."default" NOT NULL,
  creationdate timestamp without time zone NOT NULL,
  modifiedby character varying(100) COLLATE pg_catalog."default",
  lastmodificationdate timestamp without time zone,
  */
  ) {
  }
}
