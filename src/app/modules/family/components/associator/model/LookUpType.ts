export class LookUpType {
  constructor(
    public lookuptype: string = '',
    public description: string = '',
    public status: number = 0
  ) {
  }
}
