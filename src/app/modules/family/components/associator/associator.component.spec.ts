import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociatorComponent } from './associator.component';

describe('AssociatorComponent', () => {
  let component: AssociatorComponent;
  let fixture: ComponentFixture<AssociatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
