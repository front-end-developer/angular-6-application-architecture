import { Component, OnInit } from '@angular/core';
import {LookupsService} from '../../../../core/services/lookups/lookups.service';
import {LookUpType} from './model/LookUpType';
import {TypeAggregator} from './model/TypeAggregator';
import {LookUpValues} from './model/LookUpValues';
import {UserTypes} from '../../../../core/components/models/Users/UserTypes';

@Component({
  selector: 'app-associator',
  templateUrl: './associator.component.html',
  styleUrls: ['./associator.component.scss']
})
export class AssociatorComponent implements OnInit {
  public dataSource: any; // LookUpType;
  public lookUpValues: any; // LookUpValues;
  public aggregator: TypeAggregator;
  public newLookUpValues: LookUpValues;
  public keywords: string = '';
  public lookupTypeSelectedError = false;
  public sentCompletedSuccessfully = false;
  public defaultUserType: string = '--PLEASE SELECT--'; // TODO: user Internationalisation
  public displayedColumns = ['lookuptype', 'description', 'active', 'actions'];
  public lookupValuesColumns = ['lookupcode', 'description', 'active', 'actions'];
  public addValuesColumns = ['addLookupcode', 'addDescription', 'setActiveState', 'actions'];
  public loading = false;

  constructor(private lookup: LookupsService) {
    this.dataSource = [];
    this.lookUpValues = [
      {
        lookupcode: `iput`, // `<input type="text" value="" />`,
        description: `iput`,
        active: `iput`,
        actions: `<a class="btn-actions" href=""><i class="fa fa-save"></i>save</a>
            <a class="btn-actions" href=""><i class="fa fa-delete"></i>delete</a>`
      },
      {
        lookupcode: "asfasdfs",
        description: "asdfsdfasf",
        active: "asdfasff",
        actions: "button"
      },
      {
        lookupcode: "asfasdfs",
        description: "asdfsdfasf",
        active: "asdfasff",
        actions: "button"
      }
    ];
    this.aggregator = new TypeAggregator();
    // this.lookUpValues = new LookUpValues();
    this.newLookUpValues = new LookUpValues();
  }

  ngOnInit() {
    this.getTableData();
  }

  /** TODO:
   * @description Add lookup values by Type ID
   */
  addLookUpValues() {
    // this.lookUpValues = new LookUpValues();

    this.lookup.addLookUpValues(this.newLookUpValues)
      .subscribe(
        data => {
        },
        err => {
          console.log('error: ', err);
          //this.loading = false;
          // this.sentCompletedSuccessfully = false;
        }
      );
  }

  /** TODO:
   * @description get lookup values by type id
   */
  getLookUpValueByTypeId() {
    this.lookup.getLookUpValueByTypeId(123)
      .subscribe(
        data => {
        },
        err => {
          console.log('error: ', err);
          //this.loading = false;
         // this.sentCompletedSuccessfully = false;
        }
      );
  }

  getTableData() {
    this.lookup.getLookUpInformation()
      .subscribe(
        data => {
          this.loading = false;
          this.sentCompletedSuccessfully = true;
          this.dataSource = data.payload.map(function(item) {
            item.active = item.status;

            // TODO: generate dynamically
            item.actions = 'btn'; // this.generateActions(item)
            return item;
          });
        },
        err => {
          console.log('error: ', err);
          this.loading = false;
          this.sentCompletedSuccessfully = false;
        }
      );
  }

  validateSelectedService(selectedValue) {
    /*
    if (selectedValue === this.getValue(this.defaultUserType)) {
      this.lookupTypeSelectedError = true;
    } else {
      this.lookupTypeSelectedError = false;
    }
    */
  }

  getValue(string): string {
    return string.replace(/\s+/g, '_');
  }

  addNewItem(e) {}
  editItem(e) {}
  deleteItem(e) {}

  toggle(e) {
    e.preventDefault();
    const node = e.target;
    const nodeClass = node.classList;
    const anchorTag = node.getAttribute('href').substr(1);
    const tabContent = document.querySelector('#myTabContent').childNodes;
    // const tabContent = Array.from(document.querySelector('#myTabContent').childNodes);
    if (!nodeClass.contains('nav-link')) {
      return false;
    }
    e.currentTarget.childNodes.forEach(function(elem) {
      let domElement = elem.classList;
      let navLink = elem.firstElementChild.classList;
      if (domElement.contains('nav-item') && navLink.contains('nav-link') && elem.firstElementChild === node) {
        navLink.add('active');
        navLink.add('show');
      } else if (domElement.contains('nav-item') && navLink.contains('nav-link')) {
        navLink.remove('active');
        navLink.remove('show');
      }
    });

    [].forEach.call(tabContent,function(content) {
      if (content.getAttribute('id') === anchorTag) {
        content.classList.add('active');
        content.classList.add('show');
      } else {
        content.classList.remove('active');
        content.classList.remove('show');
      }
    });
  }

}
