export class API {
  private _login = 'http://localhost:3000/login';
  private _loginForgotten = 'http://localhost:3000/login/reset';
  private _loginChangePassword = 'http://localhost:3000/login/reset';
  private _admin = 'http://localhost:3000/admin';

  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }

  get loginForgotten(): string {
    return this._loginForgotten;
  }

  set loginForgotten(value: string) {
    this._loginForgotten = value;
  }

  get loginChangePassword(): string {
    return this._loginChangePassword;
  }

  set loginChangePassword(value: string) {
    this._loginChangePassword = value;
  }

  get admin(): string {
    return this._admin;
  }

  set admin(value: string) {
    this._admin = value;
  }
}
