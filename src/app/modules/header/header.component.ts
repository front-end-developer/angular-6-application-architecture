import { Component, OnInit } from '@angular/core';
import {LoginModel} from '../../core/components/models/login.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private isLoggedIn: boolean = false;
  constructor() {
    this.isLoggedIn = localStorage.getItem('jwt').length > 1;
  }

  ngOnInit() {
  }

  collapseHamburgerMenu(e) {
    const node = e.currentTarget;
    const nodeClass = node.classList;
    const childMenuClass = node.parentElement.querySelector('.navbar-collapse').classList;
    if ( nodeClass.contains('collapsed')) {
      nodeClass.remove('collapsed');
      childMenuClass.remove('show');
    } else {
      nodeClass.add('collapsed');
      childMenuClass.add('show');
    }
  }

  toggleMenu(e) {
    const node = e.currentTarget;
    const nodeClass = node.classList;
    const childMenuClass = node.querySelector('.dropdown-menu').classList;
    if ( nodeClass.contains('show')) {
      nodeClass.remove('show');
      childMenuClass.remove('show');
    } else {
      nodeClass.add('show');
      childMenuClass.add('show');
    }
  }

  loginEvent(e:LoginModel) {
  }

}
