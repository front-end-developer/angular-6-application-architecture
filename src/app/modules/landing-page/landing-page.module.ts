import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LandingPageComponent} from './components/landing-page.component';
import {LoginStandardComponent} from '../../core/components/login-standard/login-standard.component';
import {LoginService} from '@mw-services/login/login.service';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [
    LoginStandardComponent,
    LandingPageComponent
  ],
  entryComponents: [LandingPageComponent],
  providers: [LoginService]
})
export class LandingPageModule {}
