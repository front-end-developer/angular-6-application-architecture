import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ResetLoginComponent} from './components/reset-login.component';
import {ForgottenLoginView} from '../forgotten-login/components/forgotten-login.component';

const routes: Routes = [
  {
    path: 'login-reset/:sessionId',
    component: ResetLoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class ResetLoginRoutingModule {}
