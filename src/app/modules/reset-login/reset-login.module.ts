import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginService} from '@mw-services/login/login.service';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ResetLoginComponent} from './components/reset-login.component';
import {ResetPasswordComponent} from '../../core/components/login-forgotten/reset-password/reset-password.component';
// import {LoginResetModel} from '../../core/components/models/login-reset.model';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [
   //LoginResetModel,
    ResetPasswordComponent,
    ResetLoginComponent
  ],
  entryComponents: [ResetLoginComponent],
  providers: [LoginService]
})
export class ResetLoginModule {} // <- MAKE SURE THIS NAME DOES NOT CLASH
