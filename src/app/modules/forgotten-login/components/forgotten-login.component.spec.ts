import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgottenLoginView } from './forgotten-login.component';

describe('ForgottenLoginView', () => {
  let component: ForgottenLoginView;
  let fixture: ComponentFixture<ForgottenLoginView>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgottenLoginView ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgottenLoginView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
