import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ForgottenLoginView} from '../forgotten-login/components/forgotten-login.component';

const routes: Routes = [
  {
    path: 'login-restore',
    component: ForgottenLoginView
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class ForgottenLoginRoutingModule {}
