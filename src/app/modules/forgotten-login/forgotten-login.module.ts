import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginService} from '@mw-services/login/login.service';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
// import {ResetPasswordComponent} from '../../core/components/login-forgotten/reset-password/reset-password.component';
// import {LoginResetModel} from '../../core/components/models/login-reset.model';
import {ForgottenLoginView} from './components/forgotten-login.component';
import {ForgottenLoginComponent} from '../../core/components/login-forgotten/forgotten-login.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [
    ForgottenLoginComponent,
    ForgottenLoginView
  ],
  entryComponents: [ForgottenLoginView],
  providers: [LoginService]
})
export class ForgottenLoginModule {} // <- MAKE SURE THIS NAME DOES NOT CLASH
