import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
// import {ResetPasswordComponent} from '../../core/components/login-forgotten/reset-password/reset-password.component';
// import {LoginResetModel} from '../../core/components/models/login-reset.model';
import {MatButtonModule, MatCheckboxModule, MatTableModule} from '@angular/material';
import {AddOrganisationsComponent} from './components/add-organisations.component';
import {UserService} from '@mw-services/user/user.service';
import {ListAddOrganisationsComponent} from './components/list-add-organisations/list-add-organisations.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {AuthService} from '../../core/components/guards/auth/guards/auth.service';
import {ToastrService} from 'ngx-toastr';
import {FormsEditGuard} from '../../core/components/guards/forms/guard/forms-edit-guard';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularFontAwesomeModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    TranslateModule
  ],
  declarations: [
    ListAddOrganisationsComponent,
    AddOrganisationsComponent
  ],
  entryComponents: [AddOrganisationsComponent],
  providers: [UserService, ToastrService, AuthService, AuthGuard, FormsEditGuard]
})
export class AddOrganisationModule {} // <- MAKE SURE THIS NAME DOES NOT CLASH
