import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AddOrganisationsComponent} from './components/add-organisations.component';
import {UsersAddUserComponent} from '../users-add-user/components/users-add-user.component';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';

const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'organisations',
        component: AddOrganisationsComponent
      }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AddOrganisationRoutingModule {}
