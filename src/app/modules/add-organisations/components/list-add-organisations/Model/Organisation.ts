import {UserTypes} from '../../../../../core/components/models/Users/UserTypes';

export class Organisation {
  constructor(
    public idmcrportalcustomeruser: number = 0,
    public companyname: string = '',
    public city: string = '',
    public postalcode: string = '',
    public province: string = '',
    public country: string = '',
    public creationdate: string = '',
    public lastmodificationdate: string = '',
    public modifiedby: string = '',
    public firstname: string = '',
    public lastname: string = '',
    public telephone1: string = '',
    public telephone2: string = '',
    public email: string = '',
    public userType: UserTypes = UserTypes.CLIENT, // role
    public status: number = 0
  ) {
  }
}
