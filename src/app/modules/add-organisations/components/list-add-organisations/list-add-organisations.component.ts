import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../../../core/services/user/user.service';
import {ClientDetailsModel} from '../../../users-add/components/add-client-details/model/ClientDetailsModel';
import {Router} from '@angular/router';
import {Organisation} from './Model/Organisation';
import {UserTypes} from '../../../../core/components/models/Users/UserTypes';

@Component({
  selector: 'app-list-add-organisations',
  templateUrl: './list-add-organisations.component.html',
  styleUrls: ['./list-add-organisations.component.scss']
})
export class ListAddOrganisationsComponent implements OnInit {
  @ViewChild('addUsersInList') form: any;
  public dataSource: any;
  public userDataModel: Organisation;
  public sentCompletedSuccessfully = false;
  public displayedColumns = ['companyname', 'contactname', 'telephone1', 'city', 'postalcode', 'country', 'active', 'actions'];
  public loading = false;
  constructor(private userService: UserService, private router: Router) {
    this.dataSource = [];
    this.userDataModel = new Organisation();
  }

  ngOnInit() {
    this.getOrganisations();
  }

  addOrganisation(e){
    e.preventDefault();
    this.router.navigate(['admin/user']);
  }

  deleteOrganisation(e){
    e.preventDefault();
  }

  setOrganisation(e){
    // localStorage.setItem('MWPortalCustomerUserId', data.payload.portalUserId);
    // localStorage.setItem('MWPortalCustomerUserCompanyName', data.payload.companyName);
  }

  /**
   * @description generate buttons
   */
  generateActions(item: any) {}

  getOrganisations() {
    this.userService.getAllOrganisations()
      .subscribe(
        data => {
          this.loading = false;
          this.sentCompletedSuccessfully = true;
          this.dataSource = data.payload.map(function(item) {
            item.active = item.status;

            // TODO: generate dynamically
            item.actions = 'btn'; // this.generateActions(item)
            return item;
          });
        },
        err => {
          console.log('error: ', err);
          this.loading = false;
          this.sentCompletedSuccessfully = false;
        }
      );
  }

  submitModal(e) {

    // this.userDataModel you can delete this from this view
  }

}
