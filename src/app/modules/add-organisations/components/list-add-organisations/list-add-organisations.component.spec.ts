import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAddOrganisationsComponent } from './list-add-organisations.component';

describe('ListAddOrganisationsComponent', () => {
  let component: ListAddOrganisationsComponent;
  let fixture: ComponentFixture<ListAddOrganisationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAddOrganisationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAddOrganisationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
