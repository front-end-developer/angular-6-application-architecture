import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrganisationsComponent } from './add-organisations.component';

describe('AddOrganisationsComponent', () => {
  let component: AddOrganisationsComponent;
  let fixture: ComponentFixture<AddOrganisationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrganisationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrganisationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
