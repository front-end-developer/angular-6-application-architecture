import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ClientDetailsModel} from './model/ClientDetailsModel';
import {LoginModel} from '../../../../core/components/models/login.model';
import {UserTypes} from '../../../../core/components/models/Users/UserTypes';

@Component({
  selector: 'app-add-client-details',
  templateUrl: './add-client-details.component.html',
  styleUrls: ['./add-client-details.component.scss']
})
export class AddClientDetailsComponent implements OnInit, IButtonComponent {

  @ViewChild('addClientDetails') form: any;
  @Output() saveAction = new EventEmitter<any>();
  @Output() editAction = new EventEmitter<any>();
  @Output() cancelAction = new EventEmitter<any>();
  @Input() editUser: any; // TODO: GET THIS FROM A RESOLVER: via routeGuard
  public userType: UserTypes = UserTypes.ADMIN;
  public adminRole: boolean = false;
  public defaultUserType: string = '--PLEASE SELECT--';
  public hasUserTypeSelectedError = false;
  public dataModel: ClientDetailsModel;
  public listOfUserTypes: any = [
    {
      permission: this.defaultUserType
    },
    {
      permission: UserTypes.ORGANISATION
    },
    {
      permission: UserTypes.CLIENT
    },
    {
      permission: UserTypes.ADMIN
    }];

  constructor() {
    this.dataModel = new ClientDetailsModel();
    if (this.userType === UserTypes.ADMIN) {
      this.adminRole = true;
    }

    // TODO: this.editUser: to be set via RouteGuard or via JWT key
    this.editUser = {editmode: true, user: 'markwebley', email: 'asfsdadf@asfsads.com'};
    if (this.editUser.editMode) {
      // show loader
      // TODO: load services and populate model
      // loadDB.subscribe(this.editUser).pipe({ this.dataModel = data.Results; });
    }
  }

  ngOnInit() {
   // savedUser
  }

  submitForm(e) {
    e.preventDefault();
    return false;
  }

  validateSelectedService(selectedValue) {
    if (selectedValue === this.getValue(this.defaultUserType)) {
      this.hasUserTypeSelectedError = true;
    } else {
      this.hasUserTypeSelectedError = false;
    }
  }

  getValue(string): string {
    return string.replace(/\s+/g, '_');
  }

  save(e): void {
    e.preventDefault();
    const data = Object.assign({}, this.dataModel);
    delete data.password;
    this.saveAction.emit(data);
  }
  saveAndAddAnother(e): void {
    e.preventDefault();
    const data = Object.assign({}, this.dataModel);
    delete data.password;
    // TODO: save data to DB and clear form
    // this.saveAction.emit(data);
  }
  cancel(e): void {
    e.preventDefault();
    this.cancelAction.emit({});
  }
  edit(e): void {
    e.preventDefault();
    this.editAction.emit({});
  }

}
