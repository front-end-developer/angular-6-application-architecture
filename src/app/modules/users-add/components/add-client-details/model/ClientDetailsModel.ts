export class ClientDetailsModel {
  constructor(
    // public user: string = '',
    public password: string = '',
    // public email: string = '',
    // public organisationName: string = '',
    public jgzzfiscalcode: string = '',
    public firstname: string = '',
    public lastname: string = '',
    public email: string = '',
    public companyname: string = '',
    public companyid: string = '',
    public addressline1: string = '',
    public addressline2: string = '',
    public addressline3: string = '',
    public addressline4: string = '',
    public province: string = '',
    public city: string = '',
    public state: string = '',
    public country: string = '',
    public postalcode: string = '',
    public comment: string = '',
    public status: number = 1
  ) {
    //this.user = user;
  }
}
