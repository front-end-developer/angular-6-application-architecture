import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../core/services/user/user.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-users-add',
  templateUrl: './users-add.component.html',
  styleUrls: ['./users-add.component.scss']
})
export class UsersAddComponent implements OnInit {
  private loading: boolean = false;

  constructor(private router: Router, private userService: UserService, private toastr: ToastrService) { }

  ngOnInit() {
  }

  saveOrganisationDetails(e) {
    this.loading = true;
    this.userService.saveOrganisationDetails(e).subscribe(
      data => {
        this.loading = false;
        if (data.result === 'OK') {
          const userCompany = e.companyname.replace(/\s/ig, '-');
          localStorage.setItem('MWPortalCustomerUserId', data.payload.portalUserId);
          localStorage.setItem('MWPortalCustomerUserCompanyName', data.payload.companyName);
          this.router.navigate([`/admin/user/${userCompany}/add-users`]);
          this.toastr.success(`${e.companyname} has been saved`, 'Success');
        }
      },
      err => {
        console.log('error: ', err);
        this.loading = false;
      }
    );
  }

}
