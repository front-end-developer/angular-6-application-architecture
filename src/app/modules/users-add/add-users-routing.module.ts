import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UsersAddComponent} from './components/users-add.component';
import {FamilyComponent} from '../family/components/family.component';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';

const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'user',
        component: UsersAddComponent
      }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AddUsersRoutingModule {}
