import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
// import {ResetPasswordComponent} from '../../core/components/login-forgotten/reset-password/reset-password.component';
// import {LoginResetModel} from '../../core/components/models/login-reset.model';
import {MatButtonModule, MatCheckboxModule, MatTableModule} from '@angular/material';
import {UserService} from '@mw-services/user/user.service';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {UsersAddComponent} from './components/users-add.component';
import {ToastrService} from 'ngx-toastr';
import {AddClientDetailsComponent} from './components/add-client-details/add-client-details.component';
import {TranslateModule} from '@ngx-translate/core';
import {AuthService} from '../../core/components/guards/auth/guards/auth.service';
import {FormsEditGuard} from '../../core/components/guards/forms/guard/forms-edit-guard';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularFontAwesomeModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    TranslateModule
  ],
  declarations: [
    AddClientDetailsComponent,
    UsersAddComponent
  ],
  exports: [AddClientDetailsComponent],
  entryComponents: [UsersAddComponent],
  providers: [UserService, ToastrService, AuthService, AuthGuard, FormsEditGuard]
})
export class AddUsersModule {} // <- MAKE SURE THIS NAME DOES NOT CLASH
