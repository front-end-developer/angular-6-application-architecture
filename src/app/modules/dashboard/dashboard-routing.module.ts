import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ForgottenLoginView} from '../forgotten-login/components/forgotten-login.component';
import {DashboardComponent} from './components/dashboard.component';
import {UsersAddComponent} from '../users-add/components/users-add.component';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';


// should be children
const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
