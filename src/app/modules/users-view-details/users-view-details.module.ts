import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MatButtonModule, MatCheckboxModule, MatTableModule} from '@angular/material';
import {UserService} from '@mw-services/user/user.service';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {ToastrService} from 'ngx-toastr';
import {TranslateModule} from '@ngx-translate/core';
import {UsersViewDetailsComponent} from './components/users-view-details.component';
import {ViewClientDetailsComponent} from './components/view-client-details/view-client-details.component';
import {AuthService} from '../../core/components/guards/auth/guards/auth.service';
import {FormsEditGuard} from '../../core/components/guards/forms/guard/forms-edit-guard';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularFontAwesomeModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    TranslateModule
  ],
  declarations: [
    ViewClientDetailsComponent,
    UsersViewDetailsComponent
  ],
  exports: [],
  entryComponents: [UsersViewDetailsComponent],
  providers: [UserService, ToastrService, AuthService, AuthGuard, FormsEditGuard]
})
export class UsersViewDetailsModule {}
