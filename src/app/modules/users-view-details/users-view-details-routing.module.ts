import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UsersViewDetailsComponent} from './components/users-view-details.component';
import {UsersAddComponent} from '../users-add/components/users-add.component';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';

const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'admin/user/:clientid',
        component: UsersViewDetailsComponent
      }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class UsersViewDetailsRoutingModule {}
