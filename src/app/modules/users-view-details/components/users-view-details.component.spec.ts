import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersViewDetailsComponent } from './users-view-details.component';

describe('UsersViewDetailsComponent', () => {
  let component: UsersViewDetailsComponent;
  let fixture: ComponentFixture<UsersViewDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersViewDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersViewDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
