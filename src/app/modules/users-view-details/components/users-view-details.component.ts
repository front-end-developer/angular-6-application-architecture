import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users-view-details',
  templateUrl: './users-view-details.component.html',
  styleUrls: ['./users-view-details.component.scss']
})
export class UsersViewDetailsComponent implements OnInit {

  public editUser: any;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  editUserDetails(e) {
    e.user = 'markwebley';
    this.editUser = {editmode: true, user: 'markwebley', email: 'asfsdadf@asfsads.com'};
    this.router.navigate([`/admin/user/${this.editUser.user}/edit`]);

    // 1) TODO: CREATE EDIT MODULE FOR edit section, to call service to populate the form
    // 2): call service in the CREATED EDIT MODULE IT WILL USE a Service to save data to DB
    // 3): when data is saved on observable reditect to : this.router.navigate([`/admin/user/${e.user}`]);

  }
}
