import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoginModel} from '../../../../core/components/models/login.model';
import {ClientDetailsModel} from '../../../users-add/components/add-client-details/model/ClientDetailsModel';

@Component({
  selector: 'app-view-client-details',
  templateUrl: './view-client-details.component.html',
  styleUrls: ['./view-client-details.component.scss']
})
export class ViewClientDetailsComponent implements OnInit {
  @Output() editAction = new EventEmitter<any>(); // <LoginModel
  @Input() editUser: any;
  public dataModel: ClientDetailsModel;
  constructor() { }

  ngOnInit() {
  }

  edit(): void {
    const data = Object.assign({}, this.dataModel);
    delete data.password;
    this.editAction.emit(data);
  }

  toggle(e) {
    e.preventDefault();
    const node = e.target;
    const nodeClass = node.classList;
    const anchorTag = node.getAttribute('href').substr(1);
    const tabContent = document.querySelector('#myTabContent').childNodes;
    // const tabContent = Array.from(document.querySelector('#myTabContent').childNodes);
    if (!nodeClass.contains('nav-link')) {
      return false;
    }
    e.currentTarget.childNodes.forEach(function(elem) {
      let domElement = elem.classList;
      let navLink = elem.firstElementChild.classList;
      if (domElement.contains('nav-item') && navLink.contains('nav-link') && elem.firstElementChild === node) {
        navLink.add('active');
        navLink.add('show');
      } else if (domElement.contains('nav-item') && navLink.contains('nav-link')) {
        navLink.remove('active');
        navLink.remove('show');
      }
    });

    // tabContent.forEach(function(content) {
    [].forEach.call(tabContent,function(content) {
      if (content.getAttribute('id') === anchorTag) {
        content.classList.add('active');
        content.classList.add('show');
      } else {
        content.classList.remove('active');
        content.classList.remove('show');
      }
    });
  }

}
