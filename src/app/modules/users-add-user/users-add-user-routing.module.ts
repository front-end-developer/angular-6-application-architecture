import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UsersAddUserComponent} from './components/users-add-user.component';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';

const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'user/:clientid/add-users',
        component: UsersAddUserComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class UsersAddUserRoutingModule {}
