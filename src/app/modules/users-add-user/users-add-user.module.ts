import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MatButtonModule, MatCheckboxModule, MatTableModule} from '@angular/material';
import {UserService} from '@mw-services/user/user.service';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {ToastrService} from 'ngx-toastr';
import {TranslateModule} from '@ngx-translate/core';
import {UsersAddUserComponent} from './components/users-add-user.component';
import {ListAddUsersComponent} from './components/list-add-users/list-add-users.component';
import {AddUsersModalComponent} from './components/list-add-users/add-users-modal/add-users-modal.component';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';
import {AuthService} from '../../core/components/guards/auth/guards/auth.service';
import {FormsEditGuard} from '../../core/components/guards/forms/guard/forms-edit-guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularFontAwesomeModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    TranslateModule
  ],
  declarations: [
    AddUsersModalComponent,
    ListAddUsersComponent,
    UsersAddUserComponent
  ],
  exports: [ListAddUsersComponent, AddUsersModalComponent],
  entryComponents: [UsersAddUserComponent],
  providers: [UserService, ToastrService, AuthService, AuthGuard, FormsEditGuard]
})
export class UsersAddUserModule {}
