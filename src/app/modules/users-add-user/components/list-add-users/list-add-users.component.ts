import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ClientDetailsModel} from '../../../users-add/components/add-client-details/model/ClientDetailsModel';
import {UserTypes} from '../../../../core/components/models/Users/UserTypes';
import {UserService} from '../../../../core/services/user/user.service';
import {Router} from '@angular/router';
import {ClientUser} from './model/ClientUser';

@Component({
  selector: 'app-list-add-users',
  templateUrl: './list-add-users.component.html',
  styleUrls: ['./list-add-users.component.scss']
})
export class ListAddUsersComponent implements OnInit {
  @ViewChild('addUsersInList') form: any;
  @ViewChild('appAddUserModal') externalModeComponent: any;


  @Input() dataSource: any;
  @Output() saveAction = new EventEmitter<any>(); // is this still used delete if not
  public userType: UserTypes = UserTypes.ADMIN;
  public adminRole = false;
  public defaultUserType: string = '--PLEASE SELECT--';
  public hasUserTypeSelectedError = false;
  public sentCompletedSuccessfully = false;
  public dataModel: ClientDetailsModel;
  public userDataModel: ClientUser;
  public loading = false;

  // todo: make DRY
  public listOfUserTypes: any = [
    {
      permission: this.defaultUserType
    },
    {
      permission: UserTypes.ORGANISATION
    },
    {
      permission: UserTypes.CLIENT
    },
    {
      permission: UserTypes.ADMIN
    }];

  public displayedColumns = ['user', 'active', 'firstname', 'lastname', 'email', 'role', 'actions'];

  constructor(private userService: UserService, private router: Router) {
    /* for data testing
    this.dataSource = [
      // use dummy data in the meantime
      {
        user:       'mark',
        firstname:  'Marktest',
        lastname:   'Webley',
        email:      'mark.webley@barcelona-domain.com',
        role:       'Admin',
        active:   'Y'
      },
      {
        user:       'Bull',
        firstname:  'Political',
        lastname:   'Monkey',
        email:      'political.monkey@barcelona-domain.com',
        role:       'Admin',
        active:   'Y'
      },
      {
        user:       'javier',
        firstname:  'Javiertest',
        lastname:   'C',
        email:      'javier@barcelona-domain.com',
        role:       'Admin',
        active:   'Y'
      },
      {
        user:       'mickymouse',
        firstname:  'micky',
        lastname:   'mouse',
        email:      'micky.mouse@barcelona-domain.com',
        role:       'Admin',
        active:   'Y'
      },
      {
        user:       'joe',
        firstname:  'Joetest',
        lastname:   'Blogs',
        email:      'joe.blog@bbbb.com',
        role:       'Client',
        active:   'Y'
      },
      {
        user:       'sussan',
        firstname:  'Sussantest',
        lastname:   'w',
        email:      'sussan@llll.com',
        role:       'Client',
        active:   'Y'
      }
    ];
    */
    this.dataSource = [];
    this.dataModel = new ClientDetailsModel();
    this.dataModel.companyname = localStorage.getItem('MWPortalCustomerUserCompanyName');
    this.dataModel.companyid = localStorage.getItem('MWPortalCustomerUserId');
    this.userDataModel = new ClientUser();
  }

  ngOnInit() {
    // this.getListOfUsers();
    this.getUsersByPortalId();
  }


  /**
   * TODO: move to Pipes
   */
  filterBy(e) {
    //this.getListOfUsersByKeyword();
  }

  getUsersByPortalId() {
    this.userService.getUsersByPortalId(this.userDataModel)
      .subscribe(
        data => {
          this.loading = false;
          this.sentCompletedSuccessfully = true;
          this.dataSource = data.payload.map(function(item) {
            item.user = item.login;
            item.active = item.status;
            item.role = 'Client'; // item.idmcruserrole;
              /**TODO:
               * FOR imepv6.vw_get_user_by_client.idmcruserrole
               * fnd_user_role depends on three values that are foreign keys:
               * WHAT IS idmcrportalcustomerlocation first keyed in fnd_portal_customer_location
               * WHAT IS idmcrportalcustomerservice
               * WHAT IS idmcrportalcustomeruser
               */

              // TODO: generate dynamically
            item.actions = 'btn';
            return item;
          });
        },
        err => {
          console.log('error: ', err);
          this.loading = false;
          this.sentCompletedSuccessfully = false;
        }
      );
  }

  getListOfUsers() {
    this.userService.getAllUserCredentials(this.userDataModel)
      .subscribe(
        data => {
          this.loading = false;
          this.sentCompletedSuccessfully = true;
          this.dataSource = data.payload.map(function(item) {
            item.user = item.login;
            item.active = item.status;
            item.role = item.idmcrsecrole;

            // TODO: generate dynamically
            item.actions = 'btn';
            return item;
          });
        },
        err => {
          console.log('error: ', err);
          this.loading = false;
          this.sentCompletedSuccessfully = false;
        }
      );
  }

  submitForm(e) {

  }

  submitModal(e) {

    // this.userDataModel you can delete this from this view
    this.userService.createUserCredentials(e)
      .subscribe(
        data => {
          this.loading = false;
          this.sentCompletedSuccessfully = true;
          this.externalModeComponent.closeModel();
          // this.getListOfUsers();
          this.getUsersByPortalId();
        },
        err => {
          console.log('error: ', err);
          this.loading = false;
          this.sentCompletedSuccessfully = false;
        }
      );
  }

  validateSelectedService(selectedValue) {
    if (selectedValue === this.getValue(this.defaultUserType)) {
      this.hasUserTypeSelectedError = true;
    } else {
      this.hasUserTypeSelectedError = false;
    }
  }

  getValue(string): string {
    return string.replace(/\s+/g, '_');
  }
}
