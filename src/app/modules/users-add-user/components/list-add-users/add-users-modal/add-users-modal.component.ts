import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ClientUser} from '../model/ClientUser';
import {UserTypes} from '../../../../../core/components/models/Users/UserTypes';

@Component({
  selector: 'app-add-users-modal',
  templateUrl: './add-users-modal.component.html',
  styleUrls: ['./add-users-modal.component.scss']
})
export class AddUsersModalComponent implements OnInit {
  @ViewChild('content') public content: TemplateRef<any>;
  @Output() clientUserEvent = new EventEmitter<any>();
  public modalRef: NgbModalRef;
  public editMode: boolean = false;
  @Input() public dataModel: ClientUser;
  public header: string;
  public defaultUserType: string = '--PLEASE SELECT--';
  public hasUserTypeSelectedError = false;
  public listOfUserTypes: any = [
    {
      permission: this.defaultUserType
    },
    {
      permission: UserTypes.ORGANISATION
    },
    {
      permission: UserTypes.CLIENT
    },
    {
      permission: UserTypes.ADMIN
    }];

  constructor(private modalService: NgbModal) {
  }

  ngOnInit(){
  }
  fixFormDataTypes() {
    this.dataModel.status = Number(this.dataModel.status);
  }

  save(e) {
    e.preventDefault();
    this.fixFormDataTypes();
    this.clientUserEvent.emit(this.dataModel);
  }

  saveUser(e) {
    e.preventDefault();
  }

  addUser(e) {
    this.dataModel =  new ClientUser();
    this.modalRef = this.modalService.open(this.content, { size: 'lg' });
  }
  editUser(e) {
    this.editMode = true;
    // this.modalService.close(this.content);
    // this.modalRef = this.modalService.open(this.content, { size: 'lg' });
  }

  deleteUser(e) {
    e.preventDefault();
  }

  closeModel() {
    this.modalRef.close(this.dataModel);
  }

  validateSelectedService(selectedValue) {
    if (selectedValue === this.getValue(this.defaultUserType)) {
      this.hasUserTypeSelectedError = true;
    } else {
      this.hasUserTypeSelectedError = false;
    }
  }

  getValue(string): string {
    return string.replace(/\s+/g, '_');
  }
}
