import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAddUsersComponent } from './list-add-users.component';

describe('ListAddUsersComponent', () => {
  let component: ListAddUsersComponent;
  let fixture: ComponentFixture<ListAddUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAddUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAddUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
