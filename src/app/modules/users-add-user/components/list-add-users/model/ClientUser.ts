import {UserTypes} from '../../../../../core/components/models/Users/UserTypes';

export class ClientUser {
  constructor(
    public companyid: string = '',
    public companyname: string = '',
    public lastname: string = '',
    public firstname: string = '',
    public email: string = '',
    public phonenumber: string = '',
    public login: string = '', // user | username
    public password: string = '',
    public userType: UserTypes = UserTypes.CLIENT, // role
    public status: number = 0
  ) {
  }
}
