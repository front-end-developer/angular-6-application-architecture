import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {UserCompanyModel} from './model/UserCompanyModel';
import {UserTypes} from '../../../../core/components/models/Users/UserTypes';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  @ViewChild('addUser') form: any;
  public hasUserTypeSelectedError = false;
  public dataModel: UserCompanyModel;
  public userType: UserTypes;
  public defaultUserType: string = '--PLEASE SELECT--';
  public listOfUserTypes: any = [
    {
      permission: this.defaultUserType
    },
    {
      permission: UserTypes.ORGANISATION
    },
    {
      permission: UserTypes.CLIENT
    },
    {
      permission: UserTypes.ADMIN
    }];

  constructor() {
    this.dataModel = new UserCompanyModel();
  }

  ngOnInit() {
  }

  submitForm(e) {
    e.preventDefault();
    if (this.form.invalid) {
      // return;
    }
    // this.loginService.login(this.loginModel).subscribe();
  }

  validateSelectedService(selectedValue) {
    if (selectedValue === this.getValue(this.defaultUserType)) {
      this.hasUserTypeSelectedError = true;
    } else {
      this.hasUserTypeSelectedError = false;
    }
  }

  getValue(string): string {
    return string.replace(/\s+/g, '_');
  }

}
