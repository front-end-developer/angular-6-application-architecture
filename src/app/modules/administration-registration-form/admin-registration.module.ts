import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MatButtonModule, MatCheckboxModule, MatTableModule} from '@angular/material';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {ToastrService} from 'ngx-toastr';
import {TranslateModule} from '@ngx-translate/core';
import {UsersComponent} from './components/users-add.component';
import {AddClientComponent} from './components/add-client/add-client.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularFontAwesomeModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    TranslateModule
  ],
  declarations: [
    AddClientComponent,
    UsersComponent
  ],
  exports: [AddClientComponent],
  entryComponents: [UsersComponent],
  providers: [ToastrService]
})
export class AdminRegistrationModule {} // <- MAKE SURE THIS NAME DOES NOT CLASH
