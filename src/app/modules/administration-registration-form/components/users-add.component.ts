import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '@mw-services/user/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users-add.component.html',
  styleUrls: ['./users-add.component.scss']
})
export class UsersComponent implements OnInit {
  private loading: boolean = false;
  private userService: UserService;

  constructor(private router: Router) {
  }

  saveUserLogin(e) {
    this.router.navigate([`/admin/user/${e.user}/add`]);

    // TODO: still in progress
    // backend API
    /*
    this.loading = true;
    this.userService.addNewUserLogin(e).subscribe(
      data => {
        this.loading = false;
        this.router.navigate([`/admin/user/${e.user}/add`]);
      },
      err => {
        console.log('error: ', err);
        this.loading = false;
      }
    );
    */
  }

  ngOnInit() {
  }

}
