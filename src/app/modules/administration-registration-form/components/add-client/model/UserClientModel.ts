import {UserTypes} from '../../../../../core/components/models/Users/UserTypes';

export class UserClientModel {
  constructor(
    public user: string = '',
    public password: string = '',
    public email: string = '',
    public userType: UserTypes = UserTypes.CLIENT,
    public organisationName: string = ''
  ) {
    this.user = user;
  }
}
