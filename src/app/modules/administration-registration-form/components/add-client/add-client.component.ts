import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {UserClientModel} from './model/UserClientModel';
import {UserTypes} from '../../../../core/components/models/Users/UserTypes';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {

  @ViewChild('addUser') form: any;
  @Output() savedUser = new EventEmitter<any>();
  public hasUserTypeSelectedError = false;
  public dataModel: UserClientModel;
  public userType: UserTypes;
  public defaultUserType: string = '--PLEASE SELECT--';
  public listOfUserTypes: any = [
    {
      permission: this.defaultUserType
    },
    {
      permission: UserTypes.ORGANISATION
    },
    {
      permission: UserTypes.CLIENT
    },
    {
      permission: UserTypes.ADMIN
    }];

  constructor(private toastr: ToastrService) {
    this.dataModel = new UserClientModel();
  }

  ngOnInit() {
  }

  submitForm(e) {
    e.preventDefault();
    if (this.form.invalid) {
      // return;
    }
    this.save();
  }

  save(): void {
    this.toastr.success('new users added', 'Success');
    const data = Object.assign({}, this.dataModel, {sussess: true});
    delete data.password;
    this.savedUser.emit(data);
  }

  validateSelectedService(selectedValue) {
    if (selectedValue === this.getValue(this.defaultUserType)) {
      this.hasUserTypeSelectedError = true;
    } else {
      this.hasUserTypeSelectedError = false;
    }
  }

  getValue(string): string {
    return string.replace(/\s+/g, '_');
  }

}
