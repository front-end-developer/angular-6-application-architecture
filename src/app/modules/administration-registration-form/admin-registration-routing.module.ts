import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UsersComponent} from './components/users-add.component';
import {DashboardComponent} from '../dashboard/components/dashboard.component';
import {AuthGuard} from '../../core/components/guards/auth/guards/auth-guards.service';

const routes: Routes = [
  {
    path: 'admin',
    children: [
      {
        path: 'admin-registration',
        component: UsersComponent
      }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AdminRegistrationRoutingModule {}
