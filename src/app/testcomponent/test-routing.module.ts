import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TestListComponent} from './components/test-list.component';

const routes: Routes = [
  {
    path: 'component/test',
    component: TestListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class TestRoutingModule {}
