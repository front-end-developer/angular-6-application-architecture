import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TestListComponent} from './components/test-list.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TestListComponent
  ],
  exports: [
  ],
  providers: []
})
export class TestModule {}
