import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginStandardComponent } from './login-standard.component';

describe('LoginStandardComponent', () => {
  let component: LoginStandardComponent;
  let fixture: ComponentFixture<LoginStandardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginStandardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginStandardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
