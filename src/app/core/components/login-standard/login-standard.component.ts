import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {LoginModel} from '../models/login.model';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
// import {LoginService} from '@mw-services/login.service';
import {LoginService} from '../../services/login/login.service';
import * as $ from "jquery";
//import {LoginComponent} from '../login/login.component';

@Component({
  selector: 'app-login-standard',
  templateUrl: './login-standard.component.html',
  styleUrls: ['./login-standard.component.scss']
})
export class LoginStandardComponent /* extends LoginComponent */  implements OnInit {
  @ViewChild('loginFormLandingPage') form: any;
  @Output() loginAction = new EventEmitter<LoginModel>();
  public loginModel: LoginModel;
  public loginError: boolean = false;
  public sentCompletedSuccessfully: boolean = false;
  public  loading: boolean;
  constructor(protected loginService: LoginService, protected router: Router) {
    this.loginModel = new LoginModel('mark', '1234');
  }

  submitForm(e){
    e.preventDefault();
    if (this.form.invalid) {
      //return;
    }

    this.loginService.login(this.loginModel).subscribe(
      data => {
        this.loading = false;
        if (data.result === 'OK' || data.token) {
          this.sentCompletedSuccessfully = true;
          this.loginError = false;
          this.router.navigate(['admin/organisations']);
        } else {
          this.loginError = true;
        }
      },
      err => {
        console.log('error: ', err);
        this.loginError = true;
        this.loading = false;
      }
    );
  }

  // TODO check if already logged in on init or in Guard
  ngOnInit() {
  }

}
