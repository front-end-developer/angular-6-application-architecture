interface IButtonComponent {
  save(e?:any): void;

  /**
   * @description sends the user back to the readonly view
   */
  cancel(e?:any): void;
  edit(e?:any): void;
}
