export class IUser {
  id: number;
  isAdmin: boolean;
  lastLoggedIn: string; // Date;
  userType: string;
  userName: string;
}
