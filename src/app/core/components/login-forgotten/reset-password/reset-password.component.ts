import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {LoginService} from '../../../services/login/login.service';
import {NgForm} from '@angular/forms';
import {LoginResetModel} from '../../models/login-reset.model';
import * as $ from "jquery";
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  @ViewChild('resetLogin') form: any;
  @Output() forgottenLoginAction = new EventEmitter<LoginResetModel>();
  public dataModel: LoginResetModel;
  public sentCompletedSuccessfully: boolean = false;
  public restoreLoginError: boolean = false;
  public  loading: boolean;
  constructor(private loginService: LoginService, private route: ActivatedRoute) {
    this.dataModel = new LoginResetModel();
  }

  ngOnInit() {
    this.dataModel.sessionId = this.route.snapshot.paramMap.get('sessionId');
  }

  submitForm(e){
    e.preventDefault();
    if (this.form.invalid) {
      //return;
    }

    this.loginService.changePassword(this.dataModel).subscribe(
      data => {
        this.loading = false;
        if (data.result === 'OK') {
          this.sentCompletedSuccessfully = true;
          this.restoreLoginError = false;
        } else {
          this.restoreLoginError = true;
        }
      },
      err => {
        console.log('error: ', err);
        this.restoreLoginError = true;
        this.loading = false;
      }
    );
  }
}
