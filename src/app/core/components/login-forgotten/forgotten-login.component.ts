import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {LoginService} from '../../services/login/login.service';
import {NgForm} from '@angular/forms';
import {ForgottenLoginModel} from '../models/login-forgotten.model';
import * as $ from "jquery";

@Component({
  selector: 'app-forgotten-login',
  templateUrl: './forgotten-login.component.html',
  styleUrls: ['./forgotten-login.component.scss']
})
export class ForgottenLoginComponent implements OnInit {
  @ViewChild('forgottenLogin') form: any;
  @Output() forgottenLoginAction = new EventEmitter<ForgottenLoginModel>();
  public dataModel: ForgottenLoginModel;
  public sentCompletedSuccessfully: boolean = false;
  public restoreLoginError: boolean = false;
  public  loading: boolean;
  constructor(private loginService: LoginService) {
    this.dataModel = new ForgottenLoginModel('');
  }

  ngOnInit() {
  }

  submitForm(e){
    e.preventDefault();
    if (this.form.invalid) {
      return;
    }

    this.loginService.forgottenLogin(this.dataModel).subscribe(
      data => {
        this.loading = false;
        if (data.result === 'OK') {
          this.sentCompletedSuccessfully = true;
          this.restoreLoginError = false;
        } else {
          this.restoreLoginError = true;
        }
      },
      err => {
        console.log('error: ', err);
        this.sentCompletedSuccessfully = false;
        this.restoreLoginError = true;
        this.loading = false;
      }
    );
  }

}
