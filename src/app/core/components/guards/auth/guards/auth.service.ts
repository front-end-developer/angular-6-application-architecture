import {Injectable} from '@angular/core';
import {IUser} from '../../../models/Users/IUser';


@Injectable()
export class AuthService {
  currentUser: any; // IUser; // change is not an interface
  redirectUrl: string;

  constructor( /* private messageService: MessageService */) {}

  isLoggedIn(): boolean {
    return true; // !!this.currentUser;
  }

  login(userName: string, password: string): void {
    if (!userName || !password) {
      //this.messageService.addMesasge('Please enter yoru userName and password');
      return;
    }
    if (userName === 'admin') {
      this.currentUser = {
        id: 1,
        userName: userName,
        isAdmin: true,
        lastLoggedIn: new Date(),
        userType: 'ADMIN'
      };
      //this.messageService.addMessage('Admin login');
    }
  }
}
