import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';


@Injectable()
export class FormsEditGuard implements CanDeactivate<FormEditComponent> {
  /* , currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot */
  canDeactivate(component: FormEditComponent ): boolean { /* Observable<boolean> | Promise<boolean> | boolean */
    /*
     if logged in return true
     otherwise return false
   */
    return true;
  }



}
