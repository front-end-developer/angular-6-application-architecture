import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import { Router /*, CanActivate */ } from '@angular/router';
import {LoginModel} from '../models/login.model';
import {NgForm} from '@angular/forms';
import {LoginService} from '../../services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') form: any;
  @Output() loginAction = new EventEmitter<LoginModel>();
  public loginModel: LoginModel;
  public sentCompletedSuccessfully: boolean = false;
  public  loading: boolean;
  constructor(protected loginService: LoginService, protected router: Router) {
    this.loginModel = new LoginModel();
  }

  submitForm(e){
    e.preventDefault();
    if (this.form.invalid) {
      //return;
    }

    this.loginService.login(this.loginModel).subscribe(
      data => {
        this.loading = false;
        this.sentCompletedSuccessfully = true;
        this.router.navigate(['admin/dashboard']);
      },
      err => {
        console.log('error: ', err);
        this.loading = false;
      }
    );
  }

  // TODO check if already logged in on init or in Guard
  ngOnInit() {
  }

}
