import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {tap} from 'rxjs/operators';
import {Organisation} from '../../../modules/add-organisations/components/list-add-organisations/Model/Organisation';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LookupsService {

  constructor(private http: HttpClient) { }

  /**
   * IN PROGRESS
   */
  addLookUpValues(data: any): Observable<any> {
    console.log('GET url', `${environment.API_ENDPOINT}admin/lookup/values`);
    return this.http.get<any>(`${environment.API_ENDPOINT}admin/lookup/values`).pipe(
      tap(
        data => {
          // console.log('data.token:', data.token);
          // localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
  }

  /**
   * IN PROGRESS
   */
  getLookUpValueByTypeId(data: any): Observable<any> {
    console.log('GET url', `${environment.API_ENDPOINT}admin/lookup/typeid`);
    return this.http.get<any>(`${environment.API_ENDPOINT}admin/lookup/typeid`).pipe(
      tap(
        data => {
          // console.log('data.token:', data.token);
          // localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
  }

  /**
   * IN PROGRESS
   * @returns {Observable<any>}
   * @description Get all users
   */
  getLookUpInformation(): Observable<any> {
    console.log('GET url', `${environment.API_ENDPOINT}admin/lookup`);
    //  return this.http.get<Organisation>(`${environment.API_ENDPOINT}admin/organisation/users`).pipe(
    return this.http.get<any>(`${environment.API_ENDPOINT}admin/lookup`).pipe(
      tap(
        data => {
          // console.log('data.token:', data.token);
          // localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
  }
}
