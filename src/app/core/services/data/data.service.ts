/* this file for reference only */

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import {environment} from '@mw-env/environment';
import {environment} from '../../../../environments/environment';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
// import { NotificationsService } from 'angular2-notifications';

@Injectable()
export class DataService {

  constructor(
    private http: HttpClient,
   // private notifications: NotificationsService
  ) { }

  currentToken = '';
  /* We send token in the HTTPHeader 'Authorization' but because of CORS this  make each get request send an OPTION request first */
  /* Maybe we should add it in the query but that's not recommended */
  get (url, param?): Observable<any> {
    return  this.http.get<any>(environment.API_ENDPOINT + url  + this.constructFilter(param)).pipe(
      tap(
        data => {},
        error => this.unknownErrorNotification(error)
      )
    );
  }

  post (url, data?): Observable<any> {
    return this.http.post<any>(environment.API_ENDPOINT + url , data).pipe(
      tap(
        res => {},
        error => this.unknownErrorNotification(error)
      )
    );
  }

  put (url, data?): Observable<any> {
    return this.http.put<any>(environment.API_ENDPOINT + url , data).pipe(
      tap(
        res => {},
        error => this.unknownErrorNotification(error)
      )
    );
  }

  delete (url, data?): Observable<any> {
    return this.http.delete<any>(environment.API_ENDPOINT + url , data).pipe(
      tap(
        res => {},
        error => this.unknownErrorNotification(error)
      )
    );
  }


  private unknownErrorNotification(err) {
    if (err.name === 'HttpErrorResponse' && err.status === 0) {
      const title = 'Operation failed';
      const content = 'Connection lost. Please try again later.';
      const type = 'error';
      // this.notifications.create(title, content, type);
    }

  }

  private constructFilter(filterObj) {
    let filter = '';
    for (const _filter in filterObj) {
        if (filterObj.hasOwnProperty(_filter)) {
            filter = filter + '&' + _filter + '=';
              if (typeof filterObj[_filter] === 'string') {
                filter = filter + encodeURIComponent(filterObj[_filter]).replace('\'', '\\\'');
              } else {
                filter = filter + encodeURIComponent(String(filterObj[_filter]));
              }

        }
    }
    return filter;
}

}
