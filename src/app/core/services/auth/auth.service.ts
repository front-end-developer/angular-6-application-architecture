import { Injectable } from '@angular/core';
import { DataService } from '../data/data.service';

import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as jwt_decode from 'jwt-decode';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class AuthService {
  constructor(
    public jwtHelper: JwtHelperService,
    private dataService: DataService
  ) {}

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('jwt');
    // console.log('Checkin isAuthenticated');
    // console.log(!this.jwtHelper.isTokenExpired(token));
    return !this.jwtHelper.isTokenExpired(token);
  }

  public login(login: string, password: string): Observable<any> {
    return this.dataService.post('login', {
      user: login,
      password: password
    }).pipe(
      tap(
        data => {
          // TODO:set it manually here for testing
          localStorage.setItem('jwt', data.token);
          },
        error => console.log(error)
      )
    );

  }

  public logout() {
    localStorage.setItem('jwt', '');
  }

  public loginForgot(email: string) {
      return this.dataService.post('login/' + email);
    }
}
