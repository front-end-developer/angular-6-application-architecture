import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {LoginModel} from '../../components/models/login.model';
import {ClientDetailsModel} from '../../../modules/users-add/components/add-client-details/model/ClientDetailsModel';
import {ClientUser} from '../../../modules/users-add-user/components/list-add-users/model/ClientUser';
import {Organisation} from '../../../modules/add-organisations/components/list-add-organisations/Model/Organisation';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  /** DONE:
   * @description add new user login details: user (name |company name), email, password, role
   */
  createUserCredentials(formData: ClientUser): Observable<any> {
    formData.companyid = localStorage.getItem('MWPortalCustomerUserId');
    formData.companyname = localStorage.getItem('MWPortalCustomerUserCompanyName');
    const body = JSON.stringify(formData);
    return this.http.post<ClientUser>(`${environment.API_ENDPOINT}${formData.companyname}/user/credentials`, body).pipe(
      tap(
        data => {
          // localStorage.setItem('jwt', data.token);
          //console.log('data.token:', data.token);
        },
        error => console.log(error)
      )
    );
  }

  /**
   * TODO: PERHAPS not needed just filter the existing resouts
   * @param {string} keyword
   * @returns {Observable<any>}
   */
  getListOfUsersByKeyword(keyword: string): Observable<any> {
    const body = JSON.stringify(keyword);
    const formData: any = {};
    formData.companyid = localStorage.getItem('MWPortalCustomerUserId');
    formData.companyname = localStorage.getItem('MWPortalCustomerUserCompanyName');
    console.log('posting formData', keyword);
    console.log('GET url', `${environment.API_ENDPOINT}${formData.companyname}/users/search/keyword`);

    return this.http.get<ClientUser>(`${environment.API_ENDPOINT}${formData.companyname}/users/search/keyword`).pipe(
      tap(
        data => {
          // console.log('data.token:', data.token);
          // localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
  }
  /**
   * DONE
   * @returns {Observable<any>}
   * @description Get all users
   */
    getAllOrganisations(): Observable<any> {
      console.log('GET url', `${environment.API_ENDPOINT}admin/organisation/users`);
      return this.http.get<Organisation>(`${environment.API_ENDPOINT}admin/organisation/users`).pipe(
        tap(
          data => {
            // console.log('data.token:', data.token);
            // localStorage.setItem('jwt', data.token);
          },
          error => console.log(error)
        )
      );
  }

  /**
   * DONE
   * @param {ClientUser} formData
   * @returns {Observable<any>}
   * @description Get all users
   */
  getUsersByPortalId(formData: ClientUser): Observable<any> {
    formData.companyid = localStorage.getItem('MWPortalCustomerUserId');
    formData.companyname = localStorage.getItem('MWPortalCustomerUserCompanyName');
    console.log('GET url', `${environment.API_ENDPOINT}${formData.companyname}/organisation/${formData.companyid}/users`);
    return this.http.get<ClientUser>(`${environment.API_ENDPOINT}${formData.companyname}/organisation/${formData.companyid}/users`).pipe(
      tap(
        data => {
          // console.log('data.token:', data.token);
          // localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
  }

  /**
   * DONE
   * @param {ClientUser} formData
   * @returns {Observable<any>}
   * @description Get all users
   */
  getAllUserCredentials(formData: ClientUser): Observable<any> {
    const body = JSON.stringify(formData);
    console.log('posting formData', formData);
    console.log('GET url', `${environment.API_ENDPOINT}${formData.companyname}/users`);

    // TODO: PASSCOMPANY NAME FROM TEH OTHER FORM:
    formData.companyid = 'XXXX-XXXX-XXXX-XXXX-XXXX'; // <-- TODO: YOU NEED TO SEND THIS TO GET ALL INFO FOR THIS USER
    formData.companyname = 'companynametodo';

    //const httpOptions = {headers: new HttpHeaders({'Content-Type':  'application/json'})};
    //httpOptions.append();

    // body
    return this.http.get<ClientUser>(`${environment.API_ENDPOINT}${formData.companyname}/users`).pipe(
      tap(
        data => {
          // console.log('data.token:', data.token);
          // localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
  }

  /** DONE:
   * @description save user details
   */
  saveOrganisationDetails(user): Observable<any> {
    console.log('save too:', `${environment.API_ENDPOINT}${user.companyname}/details`, user);
    const body = JSON.stringify(user);
    const userCompany = user.companyname.replace(/\s/ig, '-');
    console.log('save too:', `${environment.API_ENDPOINT}${userCompany}/details`, user);
    return this.http.post<any>(`${environment.API_ENDPOINT}${userCompany}/details`, body)
    .pipe(
      tap(
        data => {
          //console.log('data.token:', data.token);
          //localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
  }

  /** TODO:
   * @description to disable user information
   */
  disableOrganisationDetails(formData): Observable<any> {
    return this.http.delete<any>(`${environment.API_ENDPOINT}${formData.companyname}/${formData.user}`).pipe(
      tap(
        data => {
          console.log('data.token:', data.token);
          localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
  }
}
