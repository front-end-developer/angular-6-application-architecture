import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';
import {LoginModel} from '../../components/models/login.model';
import {Observable, from, empty} from 'rxjs';
import {ForgottenLoginModel} from '../../components/models/login-forgotten.model';
import { catchError, map, tap } from 'rxjs/operators';
import {LoginResetModel } from '../../components/models/login-reset.model';
import {API} from '../../../modules/configs/apis';
import {environment} from '../../../../environments/environment';

//import { map, catch } from 'rxjs/operators';
//import 'rxjs/add/operator/catch';
//import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  // change to use API class
  private loginApi = 'http://localhost:3000/login';
  private adminApi = 'http://localhost:3000/admin';
  private loginForgottenApi = 'http://localhost:3000/login/reset';
  private changePasswordApi = 'http://localhost:3000/login/reset';
  constructor(private http: HttpClient) { }

  /**
   * TODO:    encrypt data before you send it down the wire
   *          api: http://localhost:3100/api/login
   *
   *          todo: encryptt the model before you send it over-the-wire
   *
   * @param {LoginModel} formData
   * @returns {Observable<any>}
   */
  login(formData: LoginModel): Observable<any> {
    const body = JSON.stringify(formData);
    console.log('posting formData', formData);
    console.log('login path:', `${environment.API_ENDPOINT}login` );
    return this.http.post<LoginModel>(`${environment.API_ENDPOINT}login`, body).pipe(
      tap(
        data => {
          console.log('data.token:', data.token);
          localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
      //.map(this.extractData)
      //.catch(this.handleError);
  }

  addUserLogin(formData: LoginModel): Observable<any> {
    const body = JSON.stringify(formData);
    console.log('posting formData', formData);
    console.log('posting url', '´${this.adminApi}/user´');
    return this.http.post<LoginModel>(`${environment.API_ENDPOINT}admin/user`, body).pipe(
      tap(
        data => {
          console.log('data.token:', data.token);
          localStorage.setItem('jwt', data.token);
        },
        error => console.log(error)
      )
    );
    //.map(this.extractData)
    //.catch(this.handleError);
  }


  public logout() {
    localStorage.setItem('jwt', '');
  }

  forgottenLogin(formData: ForgottenLoginModel): Observable<any> {
    // return this.dataService.post('login/' + email);
    const body = JSON.stringify(formData);
    console.log('posting formData', formData);
    return this.http.post<ForgottenLoginModel>(`${environment.API_ENDPOINT}login/reset`, body);
      //.map(this.extractData)
      //.catch(this.handleError);
  }

  changePassword(formData: LoginResetModel): Observable<any> {
    const isValid = this.validateParams();

    // send back a failer message in the Observable
    if (!isValid) {
      // return from<string>(['']); // TODO: needs to return an empty observable
      return empty().pipe();
    }
    const body = JSON.stringify(formData);
    console.log('posting formData', formData);
                  // API.loginChangePassword();
    console.log('call service to change actual password: ', `${this.changePasswordApi}/${formData.sessionId}`);
    return this.http.post<ForgottenLoginModel>(`${environment.API_ENDPOINT}login/reset/${formData.sessionId}`, body);
    //.map(this.extractData)
    //.catch(this.handleError);
  }

  validateParams(): boolean {
    // check password and confirm password is not the same
    // check password is not the same as the users email in db
    // check password is not the same as the username in db
    return true;
  }

  extractData(res: any) {
    //const body = res.json();
    const body = res;
    console.log('body res:', body);
    return body.fields || { };
  }

  extractLanguage(res: any) {
    const body = res.json();
    return body.data || { };
  }

  handleError(error: any) {
    console.error('post error:', error);
    return Observable.throw(error.statusText);
  }
}
