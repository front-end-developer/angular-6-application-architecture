// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  // TODO: change and set for dev when you have a dev server set up
  // remove when you have the above ready
  API_ENDPOINT: 'http://localhost:3000/',
  API_WHITE_LISTED_DOMAIN: 'localhost:3000',
  DOCUMENT_DOWNLOAD_BASE_URL: 'http://localhost:3000/assets/document/'
};
