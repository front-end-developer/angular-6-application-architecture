# Angular 6
Part of a project I was working on in Barcelona, I started it in Angular 5, then moved it to Angular 6 then created the Architecture because there was not any, and the rest is history.

# Author:
* Author: Mark Webley
* Author mail: no spam emails :)
* Author contact spanish mobile: uk whatsApp 0044 7930 743 923 | spanish mobile: 0034 631 33 61 33

## Development of Application Architecture for Business Applications - full stack
* Angular 6,
* Angular Material (data grids),
* Auth Guard,
* Auth Service etc,
* (ideomas / languages) localisation (Espanol y Ingles),
* boostrap 4,
* sass,
* css 3,
* html5,
* RXJS,
* promises,
* Toastre (i'm still a fan :) )
* nodejs,
* modular routes

## Node backend not included in this project
* nodejs,
* Knexjs,
* db seed & migration scripts,
* Hapi,
* joi,
* swagger,
* bcrypt,
* JWT, JSON Web Token,
* pgAdmin,
* postgresSQL,
* postgresSQL scripts


## TODO:
- finish Route Guards, 
- transfer management of JWT (JSON Web Token)

## Integrate RXJS like my Angular 5 and ReactJS/Redux projects
- get RID of crap promises,
- use rxjs Observables.subscribe(), ;) instead :)
- si senor :)

## My Process
* This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.
* Then I updated and change the Application Architecture to make it more componant based, modular with modular routing :).

## Generally My Architecture can start out like this, then I break it down, split it up, and make it more modular:
![Alt text](Brief-Idea-of-Front-End-Architecture.png "Front End Architecture for Angular 5 & 6 Applications")

